# **[JourneyMap for Minecraft](http://journeymap.info)** Language Files

*Language localization resources for the [JourneyMap]([http://journeymap.info) mod for Minecraft.*

## How to translate JourneyMap

1. **[Sign Up](https://bitbucket.org/account/signup/)** for a free account on BitBucket.org
1. **Review current [Issues](https://bitbucket.org/TeamJM/journeymap-lang/issues?status=new&status=open&sort=created_on)** for a list of known translation needs
1. **Read the [How to translate JourneyMap](http://journeymap.techbrew.net/help/wiki/Translate_JourneyMap_For_Your_Language) wiki page** for detailed instructions on doing a translation correctly. *Note: Changes to en_US files are generally not accepted unless they address typos.*

If you have questions or need help **after** reading all instructions on this page, join channel [#journeymap on EsperNet IRC](http://tinyurl.com/journeymapirc). Please be patient and stay connected; it may be minutes or hours before somebody sees your question.

---

You can either use your web browser to make translation changes on this site, or you can use Git if you are comfortable with it.

## Use Your Browser

This is fast and easy, especially if the files for your locale already exist.

#### How to edit an existing file

1. Browse the journeymap-lang [Source files](https://bitbucket.org/TeamJM/journeymap-lang/src) to the file you wish to change
1. Press the **Edit** button at the top of the file view.
1. Change the settings below the file editor to match the file type: (.lang = "Properties files") (.json = "JSON")
1. When your changes are complete, **Commit** your changes and provide a message explaining what you did. If your changes relate to a particular [Issue](https://bitbucket.org/TeamJM/journeymap-lang/issues?status=new&status=open&sort=created_on), please note the Issue # in your message.
1. A new Pull Request (PR) will be created and reviewed by a member of TeamJM.  If approved, it will be merged into the official source files.

#### How to **add** new files
 
You can't add new files yourself.  However, you can open an Issue to request that blank files be added for you:

1. Click [Create issue](https://bitbucket.org/TeamJM/journeymap-lang/issues/new) on the [Issues](https://bitbucket.org/TeamJM/journeymap-lang/issues?status=new&status=open&sort=created_on) page.
1. Reference the locale name (like es_ES) in the Issue title.  For example: "Create files for new en_PT translation"

Once the file(s) are created, we will update the Issue to let you know it's ready to be edited.

---

## Use Git

This requires working knowledge of git, but lets you edit files on your own PC.
 
1. [Fork](https://bitbucket.org/TeamJM/journeymap-lang/fork) this repository with a Git tool, Source Tree, etc.
1. Update/add files as needed.
1. [Open a new Pull Request](https://bitbucket.org/TeamJM/journeymap-lang/pull-requests/new) (PR). If your changes relate to a particular [Issue](https://bitbucket.org/TeamJM/journeymap-lang/issues?status=new&status=open&sort=created_on), please note the Issue # in your PR title.
1. Your new Pull Request (PR) will reviewed by a member of TeamJM.  If approved, it will be merged into the official source files.

---

## Report problems (Issues)

If the translation itself is a problem, please just edit the file and submit the change.

If you see any problems with display of a translated message (For example, there may not be enough space provided to display a certain phrase):

1. Locate the problematic message key in the [Source files](https://bitbucket.org/TeamJM/journeymap-lang/src)
1. Click [Create issue](https://bitbucket.org/TeamJM/journeymap-lang/issues/new) on the [Issues](https://bitbucket.org/TeamJM/journeymap-lang/issues?status=new&status=open&sort=created_on) page.
1. Reference the locale name (like es_ES) in the Issue name and the message key.  For example: "*Problem displaying jm.advanced.announcemod.tooltip in es_ES*"
1. Use the Issue description to describe the problem you see. For example, the message may be too long to display in the box where it is drawn.
1. Attach a screenshot file showing the problem.

## Thanks for your help!